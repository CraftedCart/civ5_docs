--- @class Player
local Player = {}

--- @param x int
--- @param y int
--- @param bump_units int|nil Works like a boolean (provide zero or non-zero) (defaults to ``1`` if not given)
---
--- @return City
function Player:InitCity(x, y) end

function Player:AcquireCity() end
function Player:KillCities() end

function Player:GetNewCityName() end

function Player:InitUnit() end
function Player:InitUnitWithNameOffset() end
function Player:DisbandUnit() end
function Player:AddFreeUnit() end

function Player:ChooseTech() end

function Player:KillUnits() end
function Player:IsHuman() end
function Player:IsBarbarian() end
function Player:GetName() end
function Player:GetNameKey() end
function Player:GetNickName() end
function Player:GetCivilizationDescription() end
function Player:GetCivilizationDescriptionKey() end
function Player:GetCivilizationShortDescription() end
function Player:GetCivilizationShortDescriptionKey() end
function Player:GetCivilizationAdjective() end
function Player:GetCivilizationAdjectiveKey() end
function Player:IsWhiteFlag() end
function Player:GetStateReligionName() end
function Player:GetStateReligionKey() end
function Player:GetWorstEnemyName() end
function Player:GetArtStyleType() end

function Player:CountCityFeatures() end
function Player:CountNumBuildings() end

function Player:GetNumWorldWonders() end
function Player:ChangeNumWorldWonders() end
function Player:GetNumWondersBeatenTo() end
function Player:SetNumWondersBeatenTo() end

function Player:IsCapitalConnectedToCity() end

function Player:IsTurnActive() end
function Player:IsSimultaneousTurns() end

function Player:FindNewCapital() end
function Player:CanRaze() end
function Player:Raze() end
function Player:Disband() end

function Player:CanReceiveGoody() end
function Player:ReceiveGoody() end
function Player:DoGoody() end
function Player:CanGetGoody() end

function Player:CanFound() end
function Player:Found() end

function Player:CanTrain() end
function Player:CanConstruct() end
function Player:CanCreate() end
function Player:CanPrepare() end
function Player:CanMaintain() end

function Player:IsCanPurchaseAnyCity() end
function Player:GetFaithPurchaseType() end
function Player:SetFaithPurchaseType() end
function Player:GetFaithPurchaseIndex() end
function Player:SetFaithPurchaseIndex() end

function Player:IsProductionMaxedUnitClass() end
function Player:IsProductionMaxedBuildingClass() end
function Player:IsProductionMaxedProject() end
function Player:GetUnitProductionNeeded() end
function Player:GetBuildingProductionNeeded() end
function Player:GetProjectProductionNeeded() end

function Player:HasReadyUnit() end
function Player:GetFirstReadyUnit() end
function Player:GetFirstReadyUnitPlot() end

function Player:HasBusyUnit() end
function Player:HasBusyMovingUnit() end

function Player:GetBuildingClassPrereqBuilding() end

function Player:RemoveBuildingClass() end

function Player:CanBuild() end
function Player:IsBuildBlockedByFeature() end
function Player:GetBestRoute() end
function Player:GetImprovementUpgradeRate() end

function Player:CalculateTotalYield() end

function Player:CalculateUnitCost() end
function Player:CalculateUnitSupply() end

function Player:GetNumMaintenanceFreeUnits() end

function Player:GetBuildingGoldMaintenance() end
function Player:SetBaseBuildingGoldMaintenance() end
function Player:ChangeBaseBuildingGoldMaintenance() end

function Player:GetImprovementGoldMaintenance() end
function Player:CalculateGoldRate() end
function Player:CalculateGoldRateTimes100() end
function Player:CalculateGrossGoldTimes100() end
function Player:CalculateInflatedCosts() end
function Player:CalculateResearchModifier() end
function Player:IsResearch() end
function Player:CanEverResearch() end
function Player:CanResearch() end
function Player:CanResearchForFree() end
function Player:GetCurrentResearch() end
function Player:IsCurrentResearchRepeat() end
function Player:IsNoResearchAvailable() end
function Player:GetResearchTurnsLeft() end
function Player:GetResearchCost() end
function Player:GetResearchProgress() end

function Player:UnitsRequiredForGoldenAge() end
function Player:UnitsGoldenAgeCapable() end
function Player:UnitsGoldenAgeReady() end
function Player:GreatGeneralThreshold() end
function Player:GreatAdmiralThreshold() end
function Player:SpecialistYield() end
function Player:SetGreatGeneralCombatBonus() end
function Player:GetGreatGeneralCombatBonus() end

function Player:GetStartingPlot() end
function Player:SetStartingPlot() end
function Player:GetTotalPopulation() end
function Player:GetAveragePopulation() end
function Player:GetRealPopulation() end

function Player:GetNewCityExtraPopulation() end
function Player:ChangeNewCityExtraPopulation() end

function Player:GetTotalLand() end
function Player:GetTotalLandScored() end

function Player:GetGold() end
function Player:SetGold() end
function Player:ChangeGold() end
function Player:CalculateGrossGold() end
function Player:GetLifetimeGrossGold() end
function Player:GetGoldFromCitiesTimes100() end
function Player:GetGoldFromCitiesMinusTradeRoutesTimes100() end
function Player:GetGoldPerTurnFromDiplomacy() end
function Player:GetCityConnectionRouteGoldTimes100() end
function Player:GetCityConnectionGold() end
function Player:GetCityConnectionGoldTimes100() end
function Player:GetGoldPerTurnFromReligion() end
function Player:GetGoldPerTurnFromTradeRoutes() end
function Player:GetGoldPerTurnFromTradeRoutesTimes100() end
function Player:GetGoldPerTurnFromTraits() end

-- Culture

function Player:GetTotalJONSCulturePerTurn() end

function Player:GetJONSCulturePerTurnFromCities() end

function Player:GetJONSCulturePerTurnFromExcessHappiness() end
function Player:GetJONSCulturePerTurnFromTraits() end

function Player:GetCultureWonderMultiplier() end

function Player:GetJONSCulturePerTurnForFree() end
function Player:ChangeJONSCulturePerTurnForFree() end

function Player:GetJONSCulturePerTurnFromMinorCivs() end
function Player:ChangeJONSCulturePerTurnFromMinorCivs() end
function Player:GetCulturePerTurnFromMinorCivs() end
function Player:GetCulturePerTurnFromMinor() end

function Player:GetCulturePerTurnFromReligion() end
function Player:GetCulturePerTurnFromBonusTurns() end
function Player:GetCultureCityModifier() end

function Player:GetJONSCulture() end
function Player:SetJONSCulture() end
function Player:ChangeJONSCulture() end

function Player:GetJONSCultureEverGenerated() end

function Player:GetLastTurnLifetimeCulture() end
function Player:GetInfluenceOn() end
function Player:GetLastTurnInfluenceOn() end
function Player:GetInfluencePerTurn() end
function Player:GetInfluenceLevel() end
function Player:GetInfluenceTrend() end
function Player:GetTurnsToInfluential() end
function Player:GetNumCivsInfluentialOn() end
function Player:GetNumCivsToBeInfluentialOn() end
function Player:GetInfluenceTradeRouteScienceBonus() end
function Player:GetInfluenceCityStateSpyRankBonus() end
function Player:GetInfluenceMajorCivSpyRankBonus() end
function Player:GetInfluenceSpyRankTooltip() end
function Player:GetTourism() end
function Player:GetTourismModifierWith() end
function Player:GetTourismModifierWithTooltip() end
function Player:GetPublicOpinionType() end
function Player:GetPublicOpinionPreferredIdeology() end
function Player:GetPublicOpinionTooltip() end
function Player:GetPublicOpinionUnhappiness() end
function Player:GetPublicOpinionUnhappinessTooltip() end

function Player:HasAvailableGreatWorkSlot() end
function Player:GetCityOfClosestGreatWorkSlot() end
function Player:GetBuildingOfClosestGreatWorkSlot() end
function Player:GetNextDigCompletePlot() end
function Player:GetWrittenArtifactCulture() end
function Player:GetNumGreatWorks() end
function Player:GetNumGreatWorkSlots() end

-- Faith

function Player:GetFaith() end
function Player:SetFaith() end
function Player:ChangeFaith() end
function Player:GetTotalFaithPerTurn() end
function Player:GetFaithPerTurnFromCities() end
function Player:GetFaithPerTurnFromMinorCivs() end
function Player:GetFaithPerTurnFromReligion() end
function Player:HasCreatedPantheon() end
function Player:GetBeliefInPantheon() end
function Player:HasCreatedReligion() end
function Player:CanCreatePantheon() end
function Player:GetReligionCreatedByPlayer() end
function Player:GetFoundedReligionEnemyCityCombatMod() end
function Player:GetFoundedReligionFriendlyCityCombatMod() end
function Player:GetMinimumFaithNextGreatProphet() end
function Player:HasReligionInMostCities() end
function Player:DoesUnitPassFaithPurchaseCheck() end

-- Happiness

function Player:GetHappiness() end
function Player:SetHappiness() end

function Player:GetExcessHappiness() end
function Player:IsEmpireUnhappy() end
function Player:IsEmpireVeryUnhappy() end
function Player:IsEmpireSuperUnhappy() end

function Player:GetHappinessFromPolicies() end
function Player:GetHappinessFromCities() end
function Player:GetHappinessFromBuildings() end

function Player:GetExtraHappinessPerCity() end
function Player:ChangeExtraHappinessPerCity() end

function Player:GetHappinessFromResources() end
function Player:GetHappinessFromResourceVariety() end
function Player:GetExtraHappinessPerLuxury() end
function Player:GetHappinessFromReligion() end
function Player:GetHappinessFromNaturalWonders() end
function Player:GetHappinessFromLeagues() end

function Player:GetUnhappiness() end
function Player:GetUnhappinessForecast() end

function Player:GetUnhappinessFromCityForUI() end

function Player:GetUnhappinessFromCityCount() end
function Player:GetUnhappinessFromCapturedCityCount() end
function Player:GetUnhappinessFromCityPopulation() end
function Player:GetUnhappinessFromCitySpecialists() end
function Player:GetUnhappinessFromOccupiedCities() end
function Player:GetUnhappinessFromPuppetCityPopulation() end
function Player:GetUnhappinessFromPublicOpinion() end
function Player:GetUnhappinessFromUnits() end
function Player:ChangeUnhappinessFromUnits() end

function Player:GetUnhappinessMod() end
function Player:GetCityCountUnhappinessMod() end
function Player:GetOccupiedPopulationUnhappinessMod() end
function Player:GetCapitalUnhappinessMod() end
function Player:GetTraitCityUnhappinessMod() end
function Player:GetTraitPopUnhappinessMod() end
function Player:IsHalfSpecialistUnhappiness() end

function Player:GetHappinessPerGarrisonedUnit() end
function Player:SetHappinessPerGarrisonedUnit() end
function Player:ChangeHappinessPerGarrisonedUnit() end

function Player:GetHappinessFromTradeRoutes() end
function Player:GetHappinessPerTradeRoute() end
function Player:SetHappinessPerTradeRoute() end
function Player:ChangeHappinessPerTradeRoute() end

function Player:GetCityConnectionTradeRouteGoldModifier() end

function Player:GetHappinessFromMinorCivs() end
function Player:GetHappinessFromMinor() end

-- END Happiness

function Player:GetBarbarianCombatBonus() end
function Player:SetBarbarianCombatBonus() end
function Player:ChangeBarbarianCombatBonus() end
function Player:GetCombatBonusVsHigherTech() end
function Player:GetCombatBonusVsLargerCiv() end

function Player:GetGarrisonedCityRangeStrikeModifier() end
function Player:ChangeGarrisonedCityRangeStrikeModifier() end

function Player:IsAlwaysSeeBarbCamps() end
function Player:SetAlwaysSeeBarbCampsCount() end
function Player:ChangeAlwaysSeeBarbCampsCount() end

function Player:IsPolicyBlocked() end
function Player:IsPolicyBranchBlocked() end
function Player:IsPolicyBranchUnlocked() end
function Player:SetPolicyBranchUnlocked() end
function Player:GetNumPolicyBranchesUnlocked() end
function Player:GetPolicyBranchChosen() end
function Player:GetNumPolicyBranchesAllowed() end
function Player:GetNumPolicies() end
function Player:GetNumPoliciesInBranch() end
function Player:HasPolicy() end
function Player:SetHasPolicy() end
function Player:GetNextPolicyCost() end
function Player:CanAdoptPolicy() end
function Player:DoAdoptPolicy() end
function Player:CanUnlockPolicyBranch() end
function Player:GetDominantPolicyBranchForTitle() end
function Player:GetLateGamePolicyTree() end
function Player:GetBranchPicked1() end
function Player:GetBranchPicked2() end
function Player:GetBranchPicked3() end

function Player:GetPolicyCatchSpiesModifier() end

function Player:GetNumPolicyBranchesFinished() end
function Player:IsPolicyBranchFinished() end

function Player:GetAvailableTenets() end
function Player:GetTenet() end

function Player:IsAnarchy() end
function Player:GetAnarchyNumTurns() end
function Player:SetAnarchyNumTurns() end
function Player:ChangeAnarchyNumTurns() end

function Player:GetAdvancedStartPoints() end
function Player:SetAdvancedStartPoints() end
function Player:ChangeAdvancedStartPoints() end
function Player:GetAdvancedStartUnitCost() end
function Player:GetAdvancedStartCityCost() end
function Player:GetAdvancedStartPopCost() end
function Player:GetAdvancedStartBuildingCost() end
function Player:GetAdvancedStartImprovementCost() end
function Player:GetAdvancedStartRouteCost() end
function Player:GetAdvancedStartTechCost() end
function Player:GetAdvancedStartVisibilityCost() end

function Player:GetAttackBonusTurns() end
function Player:GetCultureBonusTurns() end
function Player:GetTourismBonusTurns() end

function Player:GetGoldenAgeProgressThreshold() end
function Player:GetGoldenAgeProgressMeter() end
function Player:SetGoldenAgeProgressMeter() end
function Player:ChangeGoldenAgeProgressMeter() end
function Player:GetNumGoldenAges() end
function Player:SetNumGoldenAges() end
function Player:ChangeNumGoldenAges() end
function Player:GetGoldenAgeTurns() end
function Player:GetGoldenAgeLength() end
function Player:IsGoldenAge() end
function Player:ChangeGoldenAgeTurns() end
function Player:GetNumUnitGoldenAges() end
function Player:ChangeNumUnitGoldenAges() end
function Player:GetStrikeTurns() end
function Player:GetGoldenAgeModifier() end
function Player:GetGoldenAgeTourismModifier() end
function Player:GetGoldenAgeGreatWriterRateModifier() end
function Player:GetGoldenAgeGreatArtistRateModifier() end
function Player:GetGoldenAgeGreatMusicianRateModifier() end

function Player:GetHurryModifier() end

function Player:CreateGreatGeneral() end
function Player:GetGreatPeopleCreated() end
function Player:GetGreatGeneralsCreated() end
function Player:GetGreatPeopleThresholdModifier() end
function Player:GetGreatGeneralsThresholdModifier() end
function Player:GetGreatAdmiralsThresholdModifier() end
function Player:GetGreatPeopleRateModifier() end
function Player:GetGreatGeneralRateModifier() end
function Player:GetDomesticGreatGeneralRateModifier() end
function Player:GetGreatWriterRateModifier() end
function Player:GetGreatArtistRateModifier() end
function Player:GetGreatMusicianRateModifier() end
function Player:GetGreatScientistRateModifier() end
function Player:GetGreatMerchantRateModifier() end
function Player:GetGreatEngineerRateModifier() end

function Player:GetPolicyGreatPeopleRateModifier() end
function Player:GetPolicyGreatWriterRateModifier() end
function Player:GetPolicyGreatArtistRateModifier() end
function Player:GetPolicyGreatMusicianRateModifier() end
function Player:GetPolicyGreatScientistRateModifier() end
function Player:GetPolicyGreatMerchantRateModifier() end
function Player:GetPolicyGreatEngineerRateModifier() end

function Player:GetProductionModifier() end
function Player:GetUnitProductionModifier() end
function Player:GetBuildingProductionModifier() end
function Player:GetProjectProductionModifier() end
function Player:GetSpecialistProductionModifier() end
function Player:GetMaxGlobalBuildingProductionModifier() end
function Player:GetMaxTeamBuildingProductionModifier() end
function Player:GetMaxPlayerBuildingProductionModifier() end
function Player:GetFreeExperience() end
function Player:GetFeatureProductionModifier() end
function Player:GetWorkerSpeedModifier() end
function Player:GetImprovementUpgradeRateModifier() end
function Player:GetMilitaryProductionModifier() end
function Player:GetSpaceProductionModifier() end
function Player:GetSettlerProductionModifier() end
function Player:GetCapitalSettlerProductionModifier() end
function Player:GetWonderProductionModifier() end

function Player:GetUnitProductionMaintenanceMod() end
function Player:GetNumUnitsSupplied() end
function Player:GetNumUnitsSuppliedByHandicap() end
function Player:GetNumUnitsSuppliedByCities() end
function Player:GetNumUnitsSuppliedByPopulation() end
function Player:GetNumUnitsOutOfSupply() end

function Player:GetCityDefenseModifier() end
function Player:GetNumNukeUnits() end
function Player:GetNumOutsideUnits() end

function Player:GetGoldPerUnit() end
function Player:ChangeGoldPerUnitTimes100() end
function Player:GetGoldPerMilitaryUnit() end
function Player:GetExtraUnitCost() end
function Player:GetNumMilitaryUnits() end
function Player:GetHappyPerMilitaryUnit() end
function Player:IsMilitaryFoodProduction() end
function Player:GetHighestUnitLevel() end

function Player:GetConscriptCount() end
function Player:SetConscriptCount() end
function Player:ChangeConscriptCount() end

function Player:GetMaxConscript() end
function Player:GetOverflowResearch() end
function Player:GetExpInBorderModifier() end

function Player:GetLevelExperienceModifier() end

function Player:GetCultureBombTimer() end
function Player:GetConversionTimer() end

function Player:GetCapitalCity() end
function Player:IsHasLostCapital() end
function Player:GetCitiesLost() end

function Player:GetPower() end
function Player:GetMilitaryMight() end
function Player:GetTotalTimePlayed() end

function Player:GetScore() end
function Player:GetScoreFromCities() end
function Player:GetScoreFromPopulation() end
function Player:GetScoreFromLand() end
function Player:GetScoreFromWonders() end
function Player:GetScoreFromTechs() end
function Player:GetScoreFromFutureTech() end
function Player:ChangeScoreFromFutureTech() end
function Player:GetScoreFromPolicies() end
function Player:GetScoreFromGreatWorks() end
function Player:GetScoreFromReligion() end
function Player:GetScoreFromScenario1() end
function Player:ChangeScoreFromScenario1() end
function Player:GetScoreFromScenario2() end
function Player:ChangeScoreFromScenario2() end
function Player:GetScoreFromScenario3() end
function Player:ChangeScoreFromScenario3() end
function Player:GetScoreFromScenario4() end
function Player:ChangeScoreFromScenario4() end

function Player:IsGoldenAgeCultureBonusDisabled() end

function Player:IsMinorCiv() end
function Player:GetMinorCivType() end
function Player:GetMinorCivTrait() end
function Player:GetPersonality() end
function Player:IsMinorCivHasUniqueUnit() end
function Player:GetMinorCivUniqueUnit() end
function Player:SetMinorCivUniqueUnit() end
function Player:GetAlly() end
function Player:GetAlliedTurns() end
function Player:IsFriends() end
function Player:IsAllies() end
function Player:IsPlayerHasOpenBorders() end
function Player:IsPlayerHasOpenBordersAutomatically() end
function Player:GetFriendshipChangePerTurnTimes100() end
function Player:GetMinorCivFriendshipWithMajor() end
function Player:ChangeMinorCivFriendshipWithMajor() end
function Player:GetMinorCivFriendshipAnchorWithMajor() end
function Player:GetMinorCivFriendshipLevelWithMajor() end
function Player:GetActiveQuestForPlayer() end
function Player:IsMinorCivActiveQuestForPlayer() end
function Player:GetMinorCivNumActiveQuestsForPlayer() end
function Player:IsMinorCivDisplayedQuestForPlayer() end
function Player:GetMinorCivNumDisplayedQuestsForPlayer() end
function Player:GetQuestData1() end
function Player:GetQuestData2() end
function Player:GetQuestTurnsRemaining() end
function Player:IsMinorCivContestLeader() end
function Player:GetMinorCivContestValueForLeader() end
function Player:GetMinorCivContestValueForPlayer() end
function Player:IsMinorCivUnitSpawningDisabled() end
function Player:IsMinorCivRouteEstablishedWithMajor() end
function Player:IsMinorWarQuestWithMajorActive() end
function Player:GetMinorWarQuestWithMajorRemainingCount() end
function Player:IsProxyWarActiveForMajor() end
function Player:IsThreateningBarbariansEventActiveForPlayer() end
function Player:GetTurnsSinceThreatenedByBarbarians() end
function Player:GetTurnsSinceThreatenedAnnouncement() end
function Player:GetFriendshipFromGoldGift() end
function Player:GetFriendshipNeededForNextLevel() end
function Player:GetMinorCivFavoriteMajor() end
function Player:GetMinorCivScienceFriendshipBonus() end
function Player:GetMinorCivCultureFriendshipBonus() end -- DEPRECATED
function Player:GetMinorCivCurrentCultureFlatBonus() end
function Player:GetMinorCivCurrentCulturePerBuildingBonus() end
function Player:GetCurrentCultureBonus() end -- DEPRECATED
function Player:GetMinorCivCurrentCultureBonus() end
function Player:GetMinorCivHappinessFriendshipBonus() end -- DEPRECATED
function Player:GetMinorCivCurrentHappinessFlatBonus() end
function Player:GetMinorCivCurrentHappinessPerLuxuryBonus() end
function Player:GetMinorCivCurrentHappinessBonus() end
function Player:GetMinorCivCurrentFaithBonus() end
function Player:GetCurrentCapitalFoodBonus() end
function Player:GetCurrentOtherCityFoodBonus() end
function Player:GetCurrentSpawnEstimate() end
function Player:GetCurrentScienceFriendshipBonusTimes100() end
function Player:IsPeaceBlocked() end
function Player:IsMinorPermanentWar() end
function Player:GetNumMinorCivsMet() end
function Player:DoMinorLiberationByMajor() end
function Player:IsProtectedByMajor() end
function Player:CanMajorProtect() end
function Player:CanMajorStartProtection() end
function Player:CanMajorWithdrawProtection() end
function Player:GetTurnLastPledgedProtectionByMajor() end
function Player:GetTurnLastPledgeBrokenByMajor() end
function Player:GetMinorCivBullyGoldAmount() end
function Player:CanMajorBullyGold() end
function Player:GetMajorBullyGoldDetails() end
function Player:CanMajorBullyUnit() end
function Player:GetMajorBullyUnitDetails() end
function Player:CanMajorBuyout() end
function Player:GetBuyoutCost() end
function Player:CanMajorGiftTileImprovement() end
function Player:CanMajorGiftTileImprovementAtPlot() end
function Player:GetGiftTileImprovementCost() end
function Player:AddMinorCivQuestIfAble() end
function Player:GetFriendshipFromUnitGift() end

function Player:IsAlive() end
function Player:IsEverAlive() end
function Player:IsExtendedGame() end
function Player:IsFoundedFirstCity() end

function Player:GetEndTurnBlockingType() end
function Player:GetEndTurnBlockingNotificationIndex() end
function Player:HasReceivedNetTurnComplete() end
function Player:IsStrike() end

function Player:GetID() end
function Player:GetHandicapType() end
function Player:GetCivilizationType() end
function Player:GetLeaderType() end
function Player:GetPersonalityType() end
function Player:SetPersonalityType() end
function Player:GetCurrentEra() end

function Player:GetTeam() end

function Player:GetPlayerColor() end
function Player:GetPlayerColors() end

function Player:GetSeaPlotYield() end
function Player:GetYieldRateModifier() end
function Player:GetCapitalYieldRateModifier() end
function Player:GetExtraYieldThreshold() end

-- Science

function Player:GetScience() end
function Player:GetScienceTimes100() end

function Player:GetScienceFromCitiesTimes100() end
function Player:GetScienceFromOtherPlayersTimes100() end
function Player:GetScienceFromHappinessTimes100() end
function Player:GetScienceFromResearchAgreementsTimes100() end
function Player:GetScienceFromBudgetDeficitTimes100() end

-- END Science

function Player:GetProximityToPlayer() end
function Player:DoUpdateProximityToPlayer() end

function Player:GetIncomingUnitType() end
function Player:GetIncomingUnitCountdown() end

function Player:IsOption() end
function Player:SetOption() end
function Player:IsPlayable() end
function Player:SetPlayable() end

function Player:GetNumResourceUsed() end
function Player:GetNumResourceTotal() end
function Player:ChangeNumResourceTotal() end
function Player:GetNumResourceAvailable() end

function Player:GetResourceExport() end
function Player:GetResourceImport() end
function Player:GetResourceFromMinors() end

function Player:GetImprovementCount() end

function Player:IsBuildingFree() end
function Player:GetUnitClassCount() end
function Player:IsUnitClassMaxedOut() end
function Player:GetUnitClassMaking() end
function Player:GetUnitClassCountPlusMaking() end

function Player:GetBuildingClassCount() end
function Player:IsBuildingClassMaxedOut() end
function Player:GetBuildingClassMaking() end
function Player:GetBuildingClassCountPlusMaking() end
function Player:GetHurryCount() end
function Player:IsHasAccessToHurry() end
function Player:IsCanHurry() end
function Player:GetHurryGoldCost() end

-- Commented out in C++
--function Player:IsSpecialistValid() end
function Player:IsResearchingTech() end
function Player:SetResearchingTech() end

function Player:GetCombatExperience() end
function Player:ChangeCombatExperience() end
function Player:SetCombatExperience() end
function Player:GetLifetimeCombatExperience() end
function Player:GetNavalCombatExperience() end
function Player:ChangeNavalCombatExperience() end
function Player:SetNavalCombatExperience() end

function Player:GetSpecialistExtraYield() end

function Player:FindPathLength() end

function Player:GetQueuePosition() end
function Player:ClearResearchQueue() end
function Player:PushResearch() end
function Player:PopResearch() end
function Player:GetLengthResearchQueue() end
function Player:AddCityName() end
function Player:GetNumCityNames() end
function Player:GetCityName() end

function Player:Cities() end
function Player:GetNumCities() end
function Player:GetCityByID() end

function Player:Units() end
function Player:GetNumUnits() end
function Player:GetUnitByID() end

function Player:AI_updateFoundValues() end
function Player:AI_foundValue() end

function Player:GetScoreHistory() end
function Player:GetEconomyHistory() end
function Player:GetIndustryHistory() end
function Player:GetAgricultureHistory() end
function Player:GetPowerHistory() end

function Player:GetReplayData() end
function Player:SetReplayDataValue() end

function Player:GetScriptData() end
function Player:SetScriptData() end

function Player:GetNumPlots() end

function Player:GetNumPlotsBought() end
function Player:SetNumPlotsBought() end
function Player:ChangeNumPlotsBought() end

function Player:GetBuyPlotCost() end
function Player:GetPlotDanger() end

function Player:DoBeginDiploWithHuman() end
function Player:DoTradeScreenOpened() end
function Player:DoTradeScreenClosed() end
function Player:GetMajorCivApproach() end
function Player:GetApproachTowardsUsGuess() end
function Player:IsWillAcceptPeaceWithPlayer() end
function Player:IsProtectingMinor() end
function Player:IsDontSettleMessageTooSoon() end
function Player:IsStopSpyingMessageTooSoon() end
function Player:IsAskedToStopConverting() end
function Player:IsAskedToStopDigging() end
function Player:IsDoFMessageTooSoon() end
function Player:IsDoF() end
function Player:GetDoFCounter() end
function Player:IsPlayerDoFwithAnyFriend() end
function Player:IsPlayerDoFwithAnyEnemy() end
function Player:IsPlayerDenouncedFriend() end
function Player:IsPlayerDenouncedEnemy() end
function Player:IsUntrustworthyFriend() end
function Player:GetNumFriendsDenouncedBy() end
function Player:IsFriendDenouncedUs() end
function Player:GetWeDenouncedFriendCount() end
function Player:IsFriendDeclaredWarOnUs() end
function Player:GetWeDeclaredWarOnFriendCount() end
-- Commented out in C++
--function Player:IsWorkingAgainstPlayerAccepted() end
function Player:GetCoopWarAcceptedState() end
function Player:GetNumWarsFought() end

function Player:GetLandDisputeLevel() end
function Player:GetVictoryDisputeLevel() end
function Player:GetWonderDisputeLevel() end
function Player:GetMinorCivDisputeLevel() end
function Player:GetWarmongerThreat() end
function Player:IsPlayerNoSettleRequestEverAsked() end
function Player:IsPlayerStopSpyingRequestEverAsked() end
function Player:IsDemandEverMade() end
function Player:GetNumCiviliansReturnedToMe() end
function Player:GetNumLandmarksBuiltForMe() end
function Player:GetNumTimesCultureBombed() end
function Player:GetNegativeReligiousConversionPoints() end
function Player:GetNegativeArchaeologyPoints() end
function Player:HasOthersReligionInMostCities() end
function Player:IsPlayerBrokenMilitaryPromise() end
function Player:IsPlayerIgnoredMilitaryPromise() end
function Player:IsPlayerBrokenExpansionPromise() end
function Player:IsPlayerIgnoredExpansionPromise() end
function Player:IsPlayerBrokenBorderPromise() end
function Player:IsPlayerIgnoredBorderPromise() end
function Player:IsPlayerBrokenAttackCityStatePromise() end
function Player:IsPlayerIgnoredAttackCityStatePromise() end
function Player:IsPlayerBrokenBullyCityStatePromise() end
function Player:IsPlayerIgnoredBullyCityStatePromise() end
function Player:IsPlayerBrokenSpyPromise() end
function Player:IsPlayerIgnoredSpyPromise() end
function Player:IsPlayerForgivenForSpying() end
function Player:IsPlayerBrokenNoConvertPromise() end
function Player:IsPlayerIgnoredNoConvertPromise() end
function Player:IsPlayerBrokenNoDiggingPromise() end
function Player:IsPlayerIgnoredNoDiggingPromise() end
function Player:IsPlayerBrokenCoopWarPromise() end
function Player:GetOtherPlayerNumProtectedMinorsKilled() end
function Player:GetOtherPlayerNumProtectedMinorsAttacked() end
function Player:GetTurnsSincePlayerBulliedProtectedMinor() end
function Player:IsHasPlayerBulliedProtectedMinor() end
function Player:IsDenouncedPlayer() end
function Player:GetDenouncedPlayerCounter() end
function Player:IsDenouncingPlayer() end
function Player:IsPlayerRecklessExpander() end
function Player:GetRecentTradeValue() end
function Player:GetCommonFoeValue() end
function Player:GetRecentAssistValue() end
function Player:IsGaveAssistanceTo() end
function Player:IsHasPaidTributeTo() end
function Player:IsNukedBy() end
function Player:IsCapitalCapturedBy() end
function Player:IsAngryAboutProtectedMinorKilled() end
function Player:IsAngryAboutProtectedMinorAttacked() end
function Player:IsAngryAboutProtectedMinorBullied() end
function Player:IsAngryAboutSidedWithTheirProtectedMinor() end
function Player:GetNumTimesRobbedBy() end
function Player:GetNumTimesIntrigueSharedBy() end

function Player:DoForceDoF() end
function Player:DoForceDenounce() end

function Player:GetNumNotifications() end
function Player:GetNotificationStr() end
function Player:GetNotificationSummaryStr() end
function Player:GetNotificationIndex() end
function Player:GetNotificationTurn() end
function Player:GetNotificationDismissed() end
function Player:AddNotification() end

function Player:GetRecommendedWorkerPlots() end
function Player:GetRecommendedFoundCityPlots() end
function Player:GetUnimprovedAvailableLuxuryResource() end
function Player:IsAnyPlotImproved() end
function Player:GetPlayerVisiblePlot() end

function Player:GetEverPoppedGoody() end
function Player:GetClosestGoodyPlot() end
function Player:IsAnyGoodyPlotAccessible() end
function Player:GetPlotHasOrder() end
function Player:GetAnyUnitHasOrderToGoody() end
function Player:GetEverTrainedBuilder() end

function Player:GetNumFreeTechs() end
function Player:SetNumFreeTechs() end
function Player:GetNumFreePolicies() end
function Player:SetNumFreePolicies() end
function Player:ChangeNumFreePolicies() end
function Player:GetNumFreeTenets() end
function Player:SetNumFreeTenets() end
function Player:ChangeNumFreeTenets() end
function Player:GetNumFreeGreatPeople() end
function Player:SetNumFreeGreatPeople() end
function Player:ChangeNumFreeGreatPeople() end
function Player:GetNumMayaBoosts() end
function Player:SetNumMayaBoosts() end
function Player:ChangeNumMayaBoosts() end
function Player:GetNumFaithGreatPeople() end
function Player:SetNumFaithGreatPeople() end
function Player:ChangeNumFaithGreatPeople() end
function Player:GetUnitBaktun() end
function Player:IsFreeMayaGreatPersonChoice() end

function Player:GetTraitGoldenAgeCombatModifier() end
function Player:GetTraitCityStateCombatModifier() end
function Player:GetTraitGreatGeneralExtraBonus() end
function Player:GetTraitGreatScientistRateModifier() end
function Player:IsTraitBonusReligiousBelief() end
function Player:GetHappinessFromLuxury() end
function Player:IsAbleToAnnexCityStates() end
function Player:IsUsingMayaCalendar() end
function Player:GetMayaCalendarString() end
function Player:GetMayaCalendarLongString() end

function Player:GetExtraBuildingHappinessFromPolicies() end

function Player:GetNextCity() end
function Player:GetPrevCity() end

function Player:GetFreePromotionCount() end
function Player:IsFreePromotion() end
function Player:ChangeFreePromotionCount() end

function Player:GetEmbarkedGraphicOverride() end
function Player:SetEmbarkedGraphicOverride() end

function Player:AddTemporaryDominanceZone() end

function Player:GetNaturalWonderYieldModifier() end

function Player:GetPolicyBuildingClassYieldModifier() end
function Player:GetPolicyBuildingClassYieldChange() end
function Player:GetPolicyEspionageModifier() end
function Player:GetPolicyEspionageCatchSpiesModifier() end

function Player:GetPlayerBuildingClassYieldChange() end
function Player:GetPlayerBuildingClassHappiness() end

function Player:WasResurrectedBy() end
function Player:WasResurrectedThisTurnBy() end

function Player:GetOpinionTable() end
function Player:GetDealValue() end
function Player:GetDealMyValue() end
function Player:GetDealTheyreValue() end
function Player:MayNotAnnex() end

function Player:GetEspionageCityStatus() end
function Player:GetNumSpies() end
function Player:GetNumUnassignedSpies() end
function Player:GetEspionageSpies() end
function Player:HasSpyEstablishedSurveillance() end
function Player:IsSpyDiplomat() end
function Player:IsSpySchmoozing() end
function Player:CanSpyStageCoup() end
function Player:GetAvailableSpyRelocationCities() end
function Player:GetNumTechsToSteal() end
function Player:GetIntrigueMessages() end
function Player:HasRecentIntrigueAbout() end
function Player:GetRecentIntrigueInfo() end
function Player:GetCoupChanceOfSuccess() end
function Player:IsMyDiplomatVisitingThem() end
function Player:IsOtherDiplomatVisitingMe() end

function Player:GetTradeRouteRange() end
function Player:GetInternationalTradeRoutePlotToolTip() end
function Player:GetInternationalTradeRoutePlotMouseoverToolTip() end
function Player:GetNumInternationalTradeRoutesUsed() end
function Player:GetNumInternationalTradeRoutesAvailable() end
function Player:GetPotentialInternationalTradeRouteDestinations() end
function Player:GetInternationalTradeRouteBaseBonus() end
function Player:GetInternationalTradeRouteGPTBonus() end
function Player:GetInternationalTradeRouteResourceBonus() end
function Player:GetInternationalTradeRouteResourceTraitModifier() end
function Player:GetInternationalTradeRouteExclusiveBonus() end
function Player:GetInternationalTradeRouteYourBuildingBonus() end
function Player:GetInternationalTradeRouteTheirBuildingBonus() end
function Player:GetInternationalTradeRoutePolicyBonus() end
function Player:GetInternationalTradeRouteOtherTraitBonus() end
function Player:GetInternationalTradeRouteRiverModifier() end
function Player:GetInternationalTradeRouteDomainModifier() end
function Player:GetInternationalTradeRouteTotal() end
function Player:GetInternationalTradeRouteScience() end
function Player:GetPotentialTradeUnitNewHomeCity() end
function Player:GetPotentialAdmiralNewPort() end
function Player:GetNumAvailableTradeUnits() end
function Player:GetTradeUnitType() end
function Player:GetTradeYourRoutesTTString() end
function Player:GetTradeToYouRoutesTTString() end
function Player:GetTradeRoutes() end
function Player:GetTradeRoutesAvailable() end
function Player:GetTradeRoutesToYou() end
function Player:GetNumTechDifference() end

-- Culture functions. Not sure where they should go
function Player:GetGreatWorks() end
function Player:GetSwappableGreatWriting() end
function Player:GetSwappableGreatArt() end
function Player:GetSwappableGreatArtifact() end
function Player:GetSwappableGreatMusic() end
function Player:GetOthersGreatWorks() end

function Player:CanCommitVote() end
function Player:GetCommitVoteDetails() end

function Player:IsConnected() end
function Player:IsObserver() end

function Player:HasTurnTimerExpired() end

function Player:HasUnitOfClassType() end

function Player:GetWarmongerPreviewString() end
function Player:GetLiberationPreviewString() end

return Player
