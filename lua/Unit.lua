--- @class Unit
local Unit = {}

function Unit:IsNone() end
function Unit:Convert() end
function Unit:Kill() end

function Unit:IsActionRecommended() end
function Unit:IsBetterDefenderThan() end

function Unit:CanDoCommand() end
function Unit:DoCommand() end

function Unit:GetPathEndTurnPlot() end
function Unit:GeneratePath() end

function Unit:CanEnterTerritory() end
function Unit:GetDeclareWarRangeStrike() end
function Unit:CanMoveOrAttackInto() end
function Unit:CanMoveThrough() end
function Unit:JumpToNearestValidPlot() end

function Unit:GetCombatDamage() end
function Unit:GetFireSupportUnit() end

function Unit:CanAutomate() end
function Unit:CanScrap() end
function Unit:GetScrapGold() end
function Unit:CanGift() end
function Unit:CanDistanceGift() end
function Unit:CanLoadUnit() end
function Unit:CanLoad() end
function Unit:CanUnload() end
function Unit:CanUnloadAll() end
function Unit:CanHold() end
function Unit:CanSleep() end
function Unit:CanFortify() end
function Unit:CanAirPatrol() end
function Unit:IsEmbarked() end
function Unit:SetEmbarked() end
function Unit:CanHeal() end
function Unit:CanSentry() end
function Unit:CanEmbark() end
function Unit:CanDisembark() end
function Unit:CanEmbarkOnto() end
function Unit:CanDisembarkOnto() end
function Unit:CanRebaseAt() end
function Unit:Embark() end

function Unit:IsRangeAttackIgnoreLOS() end

function Unit:CanAirlift() end
function Unit:CanAirliftAt() end

function Unit:IsNukeVictim() end
function Unit:CanNuke() end
function Unit:CanNukeAt() end

function Unit:CanRangeStrike() end
function Unit:CanRangeStrikeAt() end

function Unit:CanParadrop() end
function Unit:CanParadropAt() end

function Unit:CanMakeTradeRoute() end
function Unit:CanMakeTradeRouteAt() end

function Unit:GetExoticGoodsGoldAmount() end
function Unit:GetExoticGoodsXPAmount() end

function Unit:CanPillage() end

function Unit:IsSelected() end

function Unit:CanFound() end
function Unit:CanJoin() end
function Unit:CanConstruct() end

function Unit:CanDiscover() end
function Unit:GetDiscoverAmount() end
function Unit:GetHurryProduction() end
function Unit:GetTradeGold() end
function Unit:GetTradeInfluence() end
function Unit:CanTrade() end
function Unit:CanBuyCityState() end
function Unit:CanRepairFleet() end
function Unit:CanBuildSpaceship() end

function Unit:CanGoldenAge() end
function Unit:GetGoldenAgeTurns() end
function Unit:GetGivePoliciesCulture() end
function Unit:GetBlastTourism() end
function Unit:CanBuild() end
function Unit:CanLead() end
function Unit:Lead() end
function Unit:CanGiveExperience() end
function Unit:GiveExperience() end

function Unit:CanPromote() end
function Unit:Promote() end

function Unit:GetUpgradeUnitType() end
function Unit:UpgradePrice() end
function Unit:CanUpgradeRightNow() end
function Unit:GetNumResourceNeededToUpgrade() end

function Unit:GetHandicapType() end
function Unit:GetCivilizationType() end
function Unit:GetSpecialUnitType() end
function Unit:GetCaptureUnitType() end
function Unit:GetUnitCombatType() end
function Unit:GetUnitAIType() end
function Unit:SetUnitAIType() end
function Unit:GetDomainType() end
function Unit:GetInvisibleType() end
function Unit:GetSeeInvisibleType() end
function Unit:GetDropRange() end

function Unit:FlavorValue() end
function Unit:IsBarbarian() end

function Unit:IsHuman() end
function Unit:VisibilityRange() end

function Unit:MaxMoves() end
function Unit:MovesLeft() end

function Unit:CanMove() end
function Unit:HasMoved() end
function Unit:Range() end
function Unit:NukeDamageLevel() end

function Unit:CanBuildRoute() end
function Unit:GetBuildType() end
function Unit:WorkRate() end

function Unit:IsNoBadGoodies() end
function Unit:IsOnlyDefensive() end

function Unit:IsNoCapture() end
function Unit:IsRivalTerritory() end
function Unit:IsFound() end
function Unit:IsWork() end
function Unit:IsGoldenAge() end
function Unit:CanCoexistWithEnemyUnit() end

function Unit:IsGreatPerson() end

function Unit:IsFighting() end
function Unit:IsAttacking() end
function Unit:IsDefending() end
function Unit:IsInCombat() end

function Unit:GetMaxHitPoints() end
function Unit:GetCurrHitPoints() end
function Unit:IsHurt() end
function Unit:IsDead() end
function Unit:IsDelayedDeath() end
function Unit:SetBaseCombatStrength() end
function Unit:GetBaseCombatStrength() end

function Unit:GetMaxAttackStrength() end
function Unit:GetMaxDefenseStrength() end
function Unit:GetEmbarkedUnitDefense() end

function Unit:IsCombatUnit() end
function Unit:IsCanDefend() end
function Unit:CanSiege() end
function Unit:IsCanAttackWithMove() end
function Unit:IsCanAttackRanged() end
function Unit:IsCanAttack() end
function Unit:IsCanAttackWithMoveNow() end
function Unit:IsEnemyInMovementRange() end

function Unit:IsTrade() end

function Unit:GetBaseRangedCombatStrength() end
function Unit:GetMaxRangedCombatStrength() end
function Unit:GetCombatLimit() end
function Unit:GetRangedCombatLimit() end
function Unit:CanAirAttack() end
function Unit:CanAirDefend() end
function Unit:GetAirCombatDamage() end
function Unit:GetRangeCombatDamage() end
function Unit:GetAirStrikeDefenseDamage() end
function Unit:GetBestInterceptor() end
function Unit:GetInterceptorCount() end
function Unit:GetBestSeaPillageInterceptor() end
function Unit:GetCaptureChance() end

function Unit:IsAutomated() end
function Unit:IsWaiting() end
function Unit:IsFortifyable() end
function Unit:IsEverFortifyable() end
function Unit:FortifyModifier() end
function Unit:ExperienceNeeded() end
function Unit:AttackXPValue() end
function Unit:DefenseXPValue() end
function Unit:MaxXPValue() end
function Unit:FirstStrikes() end
function Unit:ChanceFirstStrikes() end
function Unit:MaxFirstStrikes() end
function Unit:IsRanged() end
function Unit:IsMustSetUpToRangedAttack() end
function Unit:CanSetUpForRangedAttack() end
function Unit:IsSetUpForRangedAttack() end
function Unit:ImmuneToFirstStrikes() end
function Unit:NoDefensiveBonus() end
function Unit:IgnoreBuildingDefense() end
function Unit:CanMoveImpassable() end
function Unit:CanMoveAllTerrain() end
function Unit:FlatMovementCost() end
function Unit:IgnoreTerrainCost() end
function Unit:IsNeverInvisible() end
function Unit:IsInvisible() end
function Unit:IsNukeImmune() end
function Unit:IsRangeAttackOnlyInDomain() end
function Unit:IsCityAttackOnly() end

function Unit:MaxInterceptionProbability() end
function Unit:CurrInterceptionProbability() end
function Unit:EvasionProbability() end
function Unit:WithdrawalProbability() end

function Unit:GetAdjacentModifier() end
function Unit:GetAttackModifier() end
function Unit:GetDefenseModifier() end
function Unit:GetRangedAttackModifier() end
function Unit:CityAttackModifier() end
function Unit:CityDefenseModifier() end
function Unit:HillsAttackModifier() end
function Unit:HillsDefenseModifier() end
function Unit:RoughAttackModifier() end
function Unit:OpenAttackModifier() end
function Unit:OpenRangedAttackModifier() end
function Unit:OpenDefenseModifier() end
function Unit:RoughRangedAttackModifier() end
function Unit:AttackFortifiedModifier() end
function Unit:AttackWoundedModifier() end
function Unit:FlankAttackModifier() end
function Unit:RoughDefenseModifier() end
function Unit:TerrainAttackModifier() end
function Unit:TerrainDefenseModifier() end
function Unit:FeatureAttackModifier() end
function Unit:FeatureDefenseModifier() end
function Unit:UnitClassAttackModifier() end
function Unit:UnitClassDefenseModifier() end
function Unit:UnitCombatModifier() end
function Unit:DomainModifier() end
function Unit:GetStrategicResourceCombatPenalty() end
function Unit:GetUnhappinessCombatPenalty() end
function Unit:AirSweepCombatMod() end
function Unit:CapitalDefenseModifier() end
function Unit:CapitalDefenseFalloff() end

function Unit:SpecialCargo() end
function Unit:DomainCargo() end
function Unit:CargoSpace() end
function Unit:ChangeCargoSpace() end
function Unit:IsFull() end
function Unit:CargoSpaceAvailable() end
function Unit:HasCargo() end
function Unit:CanCargoAllMove() end
function Unit:GetUnitAICargo() end
function Unit:GetID() end

function Unit:GetHotKeyNumber() end
function Unit:SetHotKeyNumber() end

function Unit:GetX() end
function Unit:GetY() end
function Unit:SetXY() end
function Unit:At() end
function Unit:AtPlot() end
function Unit:GetPlot() end
function Unit:GetArea() end
function Unit:GetReconPlot() end
function Unit:SetReconPlot() end

function Unit:GetGameTurnCreated() end
function Unit:GetLastMoveTurn() end

function Unit:GetDamage() end
function Unit:SetDamage() end
function Unit:ChangeDamage() end
function Unit:GetMoves() end
function Unit:SetMoves() end
function Unit:ChangeMoves() end
function Unit:FinishMoves() end
function Unit:IsImmobile() end

function Unit:GetExperience() end
function Unit:SetExperience() end
function Unit:ChangeExperience() end
function Unit:GetLevel() end
function Unit:SetLevel() end
function Unit:ChangeLevel() end
function Unit:GetFacingDirection() end
function Unit:RotateFacingDirectionClockwise() end
function Unit:RotateFacingDirectionCounterClockwise() end
function Unit:GetCargo() end
function Unit:GetFortifyTurns() end
function Unit:GetBlitzCount() end
function Unit:IsBlitz() end
function Unit:GetAmphibCount() end
function Unit:IsAmphib() end
function Unit:GetRiverCrossingNoPenaltyCount() end
function Unit:IsRiverCrossingNoPenalty() end
function Unit:IsEnemyRoute() end
function Unit:IsAlwaysHeal() end
function Unit:IsHealOutsideFriendly() end
function Unit:IsHillsDoubleMove() end
function Unit:IsGarrisoned() end
function Unit:GetGarrisonedCity() end

function Unit:GetExtraVisibilityRange() end
function Unit:GetExtraMoves() end
function Unit:GetExtraMoveDiscount() end
function Unit:GetExtraRange() end
function Unit:GetExtraIntercept() end
function Unit:GetExtraEvasion() end
function Unit:GetExtraFirstStrikes() end
function Unit:GetExtraChanceFirstStrikes() end
function Unit:GetExtraWithdrawal() end
function Unit:GetExtraEnemyHeal() end
function Unit:GetExtraNeutralHeal() end
function Unit:GetExtraFriendlyHeal() end

function Unit:GetSameTileHeal() end
function Unit:GetAdjacentTileHeal() end

function Unit:GetExtraCombatPercent() end
function Unit:GetFriendlyLandsModifier() end
function Unit:GetFriendlyLandsAttackModifier() end
function Unit:GetOutsideFriendlyLandsModifier() end
function Unit:GetExtraCityAttackPercent() end
function Unit:GetExtraCityDefensePercent() end
function Unit:GetExtraHillsAttackPercent() end
function Unit:GetExtraHillsDefensePercent() end

function Unit:GetExtraOpenAttackPercent() end
function Unit:GetExtraOpenRangedAttackMod() end
function Unit:GetExtraRoughAttackPercent() end
function Unit:GetExtraRoughRangedAttackMod() end
function Unit:GetExtraAttackFortifiedMod() end
function Unit:GetExtraAttackWoundedMod() end
function Unit:GetExtraOpenDefensePercent() end

function Unit:GetPillageChange() end
function Unit:GetUpgradeDiscount() end
function Unit:GetExperiencePercent() end
function Unit:GetKamikazePercent() end


function Unit:IsOutOfAttacks() end
function Unit:SetMadeAttack() end
function Unit:isOutOfInterceptions() end
function Unit:SetMadeInterception() end

function Unit:IsPromotionReady() end
function Unit:SetPromotionReady() end
function Unit:GetOwner() end
function Unit:GetVisualOwner() end
function Unit:GetCombatOwner() end
function Unit:GetOriginalOwner() end
function Unit:SetOriginalOwner() end
function Unit:GetTeam() end
function Unit:GetUnitFlagIconOffset() end
function Unit:GetUnitPortraitOffset() end

function Unit:GetUnitType() end
function Unit:GetUnitClassType() end
function Unit:GetLeaderUnitType() end
function Unit:SetLeaderUnitType() end
function Unit:IsNearGreatGeneral() end
function Unit:IsStackedGreatGeneral() end
function Unit:IsIgnoreGreatGeneralBenefit() end
function Unit:GetReverseGreatGeneralModifier() end
function Unit:GetGreatGeneralCombatModifier() end
function Unit:IsNearSapper() end
function Unit:GetNearbyImprovementModifier() end
function Unit:IsFriendlyUnitAdjacent() end
function Unit:GetNumEnemyUnitsAdjacent() end
function Unit:IsEnemyCityAdjacent() end

function Unit:GetTransportUnit() end
function Unit:IsCargo() end

function Unit:GetExtraDomainModifier() end

function Unit:GetName() end
function Unit:GetNameNoDesc() end
function Unit:HasName() end
function Unit:GetNameKey() end
function Unit:SetName() end
function Unit:IsTerrainDoubleMove() end
function Unit:IsFeatureDoubleMove() end

function Unit:GetScriptData() end
function Unit:SetScriptData() end
function Unit:GetScenarioData() end
function Unit:SetScenarioData() end

function Unit:GetExtraTerrainAttackPercent() end
function Unit:GetExtraTerrainDefensePercent() end
function Unit:GetExtraFeatureAttackPercent() end
function Unit:GetExtraFeatureDefensePercent() end
function Unit:GetExtraUnitCombatModifier() end
function Unit:GetUnitClassModifier() end

function Unit:CanAcquirePromotion() end
function Unit:CanAcquirePromotionAny() end
function Unit:IsPromotionValid() end
function Unit:IsHasPromotion() end
function Unit:SetHasPromotion() end

function Unit:GetActivityType() end
function Unit:IsReadyToMove() end
function Unit:IsBusy() end

function Unit:GetReligion() end
function Unit:GetConversionStrength() end
function Unit:GetSpreadsLeft() end
function Unit:GetNumFollowersAfterSpread() end
function Unit:GetMajorityReligionAfterSpread() end

function Unit:GetTourismBlastStrength() end

function Unit:GetGreatWorkSlotType() end

-- Helper Functions
function Unit:RangeStrike() end

function Unit:PushMission() end
function Unit:PopMission() end
function Unit:LastMissionPlot() end
function Unit:CanStartMission() end

function Unit:ExecuteSpecialExploreMove() end

function Unit:SetDeployFromOperationTurn() end
function Unit:IsHigherTechThan() end
function Unit:IsLargerCivThan() end

function Unit:IsRangedSupportFire() end

return Player
