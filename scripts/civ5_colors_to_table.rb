#!/usr/bin/env ruby

require "nokogiri"

def main
  abort "Usage: #{$0} path/to/CIV5Colors.xml" if ARGV.length < 1

  content = File.read(ARGV[0])
  xml = Nokogiri::XML(content)

  puts <<~EOF
    .. list-table::
       :header-rows: 1
       :widths: 20 80 1 1 1 1

       * - Example
         - Name
         - R
         - G
         - B
         - A

  EOF

  xml.xpath("/GameData/Colors/Row").each do |elem|
    color_name = elem.at_xpath("Type").content
    red = elem.at_xpath("Red").content.to_f
    green = elem.at_xpath("Green").content.to_f
    blue = elem.at_xpath("Blue").content.to_f
    alpha = elem.at_xpath("Alpha").content.to_f

    puts "   * - :civ5_#{color_name.downcase}:`Example text`"
    puts "     - ``#{color_name}``"
    puts "     - #{red}"
    puts "     - #{green}"
    puts "     - #{blue}"
    puts "     - #{alpha}"
  end
end

if __FILE__ == $0
  main
end

