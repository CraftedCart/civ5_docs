Acknowledgements
================

.. contents:: :local:

These docs are currently maintained by CraftedCart, though it would not be possible without the resources of others.

Other articles
--------------

- `<http://modiki.civfanatics.com/index.php?title=Text_icons_and_markups_(Civ5)>`_

Tools to extract game files
---------------------------

- Dragon UnPACKer: https://www.elberethzone.net/dragon-unpacker.html (Can unpack .fpk files, found in ``Resources``)
- DDS unpacker: https://forums.civfanatics.com/threads/dds-unpacker-for-interface-textures.389316/ (Decompresses various
  DDS files found inside .fpk files)
