Database tables
===============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   table_defines
   table_game_speeds
   table_projects
