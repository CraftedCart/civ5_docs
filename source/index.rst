Welcome to the unofficial Civilization 5 modding docs
=====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   lua_api/index
   db_tables/index
   text_formatting
   expansions
   acknowledgements

Indices and tables
==================

- :ref:`genindex`
- :ref:`search`

.. - :ref:`modindex`

Contributing
============

Want to help contribute to these docs? The source is available at https://gitlab.com/CraftedCart/civ5_docs
