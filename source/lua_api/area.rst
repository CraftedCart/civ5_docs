Area
====

.. contents:: :local:

Description
-----------

An area of hex tiles

API reference
-------------

.. lua:autoclass:: Area
