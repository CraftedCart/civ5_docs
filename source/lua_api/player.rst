Player
======

.. contents:: :local:

Description
-----------

A player (Human or AI)

API reference
-------------

.. lua:autoclass:: Player
